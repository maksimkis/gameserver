//
//  TCPServer.h
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//  Copyright © 2015 Crazzy. All rights reserved.
//

#ifndef TCPServer_h
#define TCPServer_h

class TCPServer : public TCPSocket {
public:
    TCPServer();
    virtual ~TCPServer();
    
    virtual bool Listen(string address, uint16_t port);
    virtual TCPSocket *Accept(fd_set *fd);
};
#endif /* TCPServer_h */
