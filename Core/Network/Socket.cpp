//
//  Socket.cpp
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//
//

#include "includes.h"
#include "util.h"
#include "Socket.h"
#include <string.h>

#ifndef WIN32
    int GetLastError( ) { return errno; }
#endif

Socket::Socket():
m_socket(INVALID_SOCKET),
m_hasError(false),
m_error(0) {
    memset( &m_sockAddrIn, 0, sizeof(m_sockAddrIn));
}

Socket::Socket(SOCKET socket, sockaddr_in sock_addr_in):
m_socket(socket),
m_sockAddrIn(sock_addr_in),
m_hasError(false),
m_error(0) {
}

Socket::~Socket() {
    if( m_socket != INVALID_SOCKET )
        closesocket(m_socket);
}

BYTEARRAY Socket::getPort() {
    return UTIL_CreateByteArray( m_sockAddrIn.sin_port, false);
}

BYTEARRAY Socket::getIp() {
    return UTIL_CreateByteArray((uint32_t)m_sockAddrIn.sin_addr.s_addr, false);
}

string Socket::getIpString() {
    return inet_ntoa(m_sockAddrIn.sin_addr);
}

string Socket::getErrorString() {
    if( !m_hasError )
        return "NO ERROR";
    
    switch(m_error) {
        case EWOULDBLOCK: return "EWOULDBLOCK";
        case EINPROGRESS: return "EINPROGRESS";
        case EALREADY: return "EALREADY";
        case ENOTSOCK: return "ENOTSOCK";
        case EDESTADDRREQ: return "EDESTADDRREQ";
        case EMSGSIZE: return "EMSGSIZE";
        case EPROTOTYPE: return "EPROTOTYPE";
        case ENOPROTOOPT: return "ENOPROTOOPT";
        case EPROTONOSUPPORT: return "EPROTONOSUPPORT";
        case ESOCKTNOSUPPORT: return "ESOCKTNOSUPPORT";
        case EOPNOTSUPP: return "EOPNOTSUPP";
        case EPFNOSUPPORT: return "EPFNOSUPPORT";
        case EAFNOSUPPORT: return "EAFNOSUPPORT";
        case EADDRINUSE: return "EADDRINUSE";
        case EADDRNOTAVAIL: return "EADDRNOTAVAIL";
        case ENETDOWN: return "ENETDOWN";
        case ENETUNREACH: return "ENETUNREACH";
        case ENETRESET: return "ENETRESET";
        case ECONNABORTED: return "ECONNABORTED";
        case ECONNRESET: return "ECONNRESET";
        case ENOBUFS: return "ENOBUFS";
        case EISCONN: return "EISCONN";
        case ENOTCONN: return "ENOTCONN";
        case ESHUTDOWN: return "ESHUTDOWN";
        case ETOOMANYREFS: return "ETOOMANYREFS";
        case ETIMEDOUT: return "ETIMEDOUT";
        case ECONNREFUSED: return "ECONNREFUSED";
        case ELOOP: return "ELOOP";
        case ENAMETOOLONG: return "ENAMETOOLONG";
        case EHOSTDOWN: return "EHOSTDOWN";
        case EHOSTUNREACH: return "EHOSTUNREACH";
        case ENOTEMPTY: return "ENOTEMPTY";
        case EUSERS: return "EUSERS";
        case EDQUOT: return "EDQUOT";
        case ESTALE: return "ESTALE";
        case EREMOTE: return "EREMOTE";
    }
    
    return "UNKNOWN ERROR (" + UTIL_ToString( m_error ) + ")";
}

void Socket::setFD(fd_set *fd, fd_set *send_fd, int *nfds) {
    if(m_socket == INVALID_SOCKET)
        return;
    
    FD_SET(m_socket, fd);
    FD_SET(m_socket, send_fd);
    
#ifndef WIN32
    if(m_socket > *nfds)
        *nfds = m_socket;
#endif
}

void Socket::allocate(int type) {
    m_socket = socket(AF_INET, type, 0);

    if(m_socket == INVALID_SOCKET) {
        m_hasError = true;
        m_error = GetLastError();
        CONSOLE_Print( "[SOCKET] error (socket) - " + this->getErrorString());
        return;
    }
}

void Socket::reset() {
    if(m_socket != INVALID_SOCKET )
        closesocket(m_socket);
    
    m_socket = INVALID_SOCKET;
    memset( &m_sockAddrIn, 0, sizeof(m_sockAddrIn));
    m_hasError = false;
    m_error = 0;
}

