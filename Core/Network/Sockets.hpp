/*
 *  Sockets.hpp
 *  Sockets
 *
 *  Created by Crazzy on 27.11.15.
 *  Copyright © 2015 Crazzy. All rights reserved.
 *
 */

#ifndef Sockets_
#define Sockets_

/* The classes below are exported */
#pragma GCC visibility push(default)

class Sockets
{
	public:
		void HelloWorld(const char *);
};

#pragma GCC visibility pop
#endif
