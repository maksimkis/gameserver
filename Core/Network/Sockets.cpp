/*
 *  Sockets.cpp
 *  Sockets
 *
 *  Created by Crazzy on 27.11.15.
 *  Copyright © 2015 Crazzy. All rights reserved.
 *
 */

#include <iostream>
#include "Sockets.hpp"
#include "SocketsPriv.hpp"

void Sockets::HelloWorld(const char * s)
{
	 SocketsPriv *theObj = new SocketsPriv;
	 theObj->HelloWorldPriv(s);
	 delete theObj;
};

void SocketsPriv::HelloWorldPriv(const char * s) 
{
	std::cout << s << std::endl;
};

