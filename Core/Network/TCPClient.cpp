//
//  TCPClient.cpp
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//  Copyright © 2015 Crazzy. All rights reserved.
//

#include "Socket.h"
#include "includes.h"
#include "util.h"
#include "TCPSocket.h"
#include "TCPClient.h"


TCPClient::TCPClient() :
TCPSocket(),
m_connecting(false) {
}

TCPClient::~TCPClient() {
}

void TCPClient::reset() {
    TCPSocket::reset();
    m_connecting = false;
}

void TCPClient::disconnect() {
    TCPSocket::disconnect();
    m_connecting = false;
}

void TCPClient::connect(string localaddress, string address, uint16_t port) {
    if(m_socket == INVALID_SOCKET || m_hasError || m_connecting || m_connected)
        return;
    
    if(!localaddress.empty()) {
        struct sockaddr_in LocalSIN;
        memset(&LocalSIN, 0, sizeof(LocalSIN));
        LocalSIN.sin_family = AF_INET;
        
        if((LocalSIN.sin_addr.s_addr = inet_addr(localaddress.c_str())) == INADDR_NONE)
            LocalSIN.sin_addr.s_addr = INADDR_ANY;
        
        LocalSIN.sin_port = htons(0);
        if(::bind(m_socket, (struct sockaddr *)&LocalSIN, sizeof(LocalSIN)) == SOCKET_ERROR) {
            m_hasError = true;
            m_error = GetLastError();
            CONSOLE_Print("[TCPCLIENT] error (bind) - " + getErrorString());
            return;
        }
    }
    
    // get IP address
    
    struct hostent *HostInfo;
    uint32_t HostAddress;
    HostInfo = gethostbyname(address.c_str());
    
    if(!HostInfo) {
        m_hasError = true;
        // m_Error = h_error;
        CONSOLE_Print("[TCPCLIENT] error (gethostbyname)");
        return;
    }
    
    memcpy(&HostAddress, HostInfo->h_addr, HostInfo->h_length);
    
    // connect
    
    m_sockAddrIn.sin_family = AF_INET;
    m_sockAddrIn.sin_addr.s_addr = HostAddress;
    m_sockAddrIn.sin_port = htons(port);
    
    if(::connect(m_socket, (struct sockaddr *)&m_sockAddrIn, sizeof(m_sockAddrIn)) == SOCKET_ERROR) {
        if(GetLastError() != EINPROGRESS && GetLastError() != EWOULDBLOCK) {
            // connect error
            
            m_hasError = true;
            m_error = GetLastError();
            CONSOLE_Print("[TCPCLIENT] error (connect) - " + getErrorString());
            return;
        }
    }
    
    m_connecting = true;
}

bool TCPClient::checkConnect() {
    if(m_socket == INVALID_SOCKET || m_hasError || !m_connecting)
        return false;
    
    fd_set fd;
    FD_ZERO(&fd);
    FD_SET(m_socket, &fd);
    
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    
    // check if the socket is connected
    
#ifdef WIN32
    if(select(1, NULL, &fd, NULL, &tv) == SOCKET_ERROR) {
#else
    if(select(m_socket + 1, NULL, &fd, NULL, &tv) == SOCKET_ERROR) {
#endif
        m_hasError = true;
        m_error = GetLastError();
        return false;
    }
    
    if(FD_ISSET(m_socket, &fd)) {
        m_connecting = false;
        m_connected = true;
        return true;
    }
    
    return false;
}