//
//  TCPServer.cpp
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//  Copyright © 2015 Crazzy. All rights reserved.
//

#include "Socket.h"
#include "includes.h"
#include "util.h"
#include "TCPSocket.h"
#include "TCPServer.h"

TCPServer::TCPServer():
TCPSocket() {
    // set the socket to reuse the address in case it hasn't been released yet
    int optval = 1;
#ifdef WIN32
    setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, (const char *)&optval, sizeof(int));
#else
    setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval, sizeof(int));
#endif
}

TCPServer::~TCPServer() {
}

bool TCPServer::Listen(string address, uint16_t port) {
    if(m_socket == INVALID_SOCKET || m_hasError)
        return false;
    
    m_sockAddrIn.sin_family = AF_INET;
    
    if(!address.empty()) {
        if((m_sockAddrIn.sin_addr.s_addr = inet_addr(address.c_str())) == INADDR_NONE)
            m_sockAddrIn.sin_addr.s_addr = INADDR_ANY;
    }
    else
        m_sockAddrIn.sin_addr.s_addr = INADDR_ANY;
    
    m_sockAddrIn.sin_port = htons(port);
    
    if(::bind(m_socket, (struct sockaddr *)&m_sockAddrIn, sizeof(m_sockAddrIn)) == SOCKET_ERROR) {
        m_hasError = true;
        m_error = GetLastError();
        CONSOLE_Print("[TCPSERVER] error (bind) - " + getErrorString());
        return false;
    }
    
    // listen, queue length 8
    if(listen(m_socket, 8) == SOCKET_ERROR) {
        m_hasError = true;
        m_error = GetLastError();
        CONSOLE_Print("[TCPSERVER] error (listen) - " + getErrorString());
        return false;
    }
    
    return true;
}

TCPSocket *TCPServer::Accept(fd_set *fd) {
    if(m_socket == INVALID_SOCKET || m_hasError)
        return NULL;
    
    if(FD_ISSET(m_socket, fd)) {
        // a connection is waiting, accept it
        
        struct sockaddr_in Addr;
        int AddrLen = sizeof(Addr);
        SOCKET NewSocket;
        
#ifdef WIN32
        if((NewSocket = accept(m_socket, (struct sockaddr *)&Addr, &AddrLen)) == INVALID_SOCKET) {
#else
            if((NewSocket = accept(m_socket, (struct sockaddr *)&Addr, (socklen_t *)&AddrLen)) == INVALID_SOCKET) {
#endif
                // accept error, ignore it
            }
            else {
                // success! return the new socket
                
                return new TCPSocket(NewSocket, Addr);
            }
    }
    
    return nullptr;
}