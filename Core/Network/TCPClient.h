//
//  TCPClient.h
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//  Copyright © 2015 Crazzy. All rights reserved.
//

#ifndef TCPClient_h
#define TCPClient_h

class TCPClient : public TCPSocket {
public:
    TCPClient();
    virtual ~TCPClient();
    virtual void reset();
    virtual void disconnect();
    virtual bool getConnecting() {return m_connecting;}
    virtual void connect(string localaddress, string address, uint16_t port);
    virtual bool checkConnect();
    
protected:
    bool m_connecting;
};

#endif /* TCPClient_h */
