//
//  Socket.h
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//  
//

#ifndef Socket_h
#define Socket_h

#include <stdio.h>
#include "includes.h"


#ifdef WIN32
#include <winsock2.h>
#include <errno.h>

#define EADDRINUSE WSAEADDRINUSE
#define EADDRNOTAVAIL WSAEADDRNOTAVAIL
#define EAFNOSUPPORT WSAEAFNOSUPPORT
#define EALREADY WSAEALREADY
#define ECONNABORTED WSAECONNABORTED
#define ECONNREFUSED WSAECONNREFUSED
#define ECONNRESET WSAECONNRESET
#define EDESTADDRREQ WSAEDESTADDRREQ
#define EDQUOT WSAEDQUOT
#define EHOSTDOWN WSAEHOSTDOWN
#define EHOSTUNREACH WSAEHOSTUNREACH
#define EINPROGRESS WSAEINPROGRESS
#define EISCONN WSAEISCONN
#define ELOOP WSAELOOP
#define EMSGSIZE WSAEMSGSIZE
// #define ENAMETOOLONG WSAENAMETOOLONG
#define ENETDOWN WSAENETDOWN
#define ENETRESET WSAENETRESET
#define ENETUNREACH WSAENETUNREACH
#define ENOBUFS WSAENOBUFS
#define ENOPROTOOPT WSAENOPROTOOPT
#define ENOTCONN WSAENOTCONN
// #define ENOTEMPTY WSAENOTEMPTY
#define ENOTSOCK WSAENOTSOCK
#define EOPNOTSUPP WSAEOPNOTSUPP
#define EPFNOSUPPORT WSAEPFNOSUPPORT
#define EPROTONOSUPPORT WSAEPROTONOSUPPORT
#define EPROTOTYPE WSAEPROTOTYPE
#define EREMOTE WSAEREMOTE
#define ESHUTDOWN WSAESHUTDOWN
#define ESOCKTNOSUPPORT WSAESOCKTNOSUPPORT
#define ESTALE WSAESTALE
#define ETIMEDOUT WSAETIMEDOUT
#define ETOOMANYREFS WSAETOOMANYREFS
#define EUSERS WSAEUSERS
#define EWOULDBLOCK WSAEWOULDBLOCK
#else
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

typedef int SOCKET;

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

#define closesocket close

extern int GetLastError( );
#endif

#ifndef INADDR_NONE
#define INADDR_NONE -1
#endif

#ifndef MSG_NOSIGNAL
#define MSG_NOSIGNAL 0
#endif

#ifdef WIN32
#define SHUT_RDWR 2
#endif

class Socket {
public:
    Socket();
    Socket(SOCKET socket, sockaddr_in sock_addr_in );
    ~Socket();
    
    virtual BYTEARRAY getPort();
    virtual BYTEARRAY getIp();
    virtual string getIpString();
    virtual bool hasError()	{ return m_hasError; }
    virtual int getError() { return m_error; }
    virtual string getErrorString();
    virtual void setFD(fd_set *fd, fd_set *send_fd, int *nfds);
    virtual void allocate( int type );
    virtual void reset();
    
protected:
    SOCKET m_socket;
    struct sockaddr_in m_sockAddrIn;
    bool m_hasError;
    int m_error;
};

#endif /* Socket_h */
