//
//  TCPSocket.h
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//  Copyright © 2015 Crazzy. All rights reserved.
//

#ifndef TCPSocket_h
#define TCPSocket_h

class TCPSocket : public Socket {
public:
    TCPSocket();
    TCPSocket(SOCKET socket, struct sockaddr_in sock_addr_in);
    virtual ~TCPSocket();
    virtual void reset();
    virtual bool getConnected()				{ return m_connected; }
    virtual string *getBytes()					{ return &m_recvBuffer; }
    virtual void putBytes(string bytes);
    virtual void putBytes(BYTEARRAY bytes);
    virtual void clearRecvBuffer()				{ m_recvBuffer.clear( ); }
    virtual void clearSendBuffer()				{ m_sendBuffer.clear( ); }
    virtual uint32_t getLastRecv()				{ return m_lastRecv; }
    virtual uint32_t getLastSend()				{ return m_lastSend; }
    virtual void doRecv(fd_set *fd);
    virtual void doSend(fd_set *send_fd);
    virtual void disconnect();
    virtual void setNoDelay(bool noDelay);
    virtual void setLogFile(string nLogFile)	{ m_LogFile = nLogFile; }
    virtual void setNonBlock();
protected:
    bool m_connected;
    string m_LogFile;
    
private:
    string m_recvBuffer;
    string m_sendBuffer;
    uint32_t m_lastRecv;
    uint32_t m_lastSend;
};
#endif /* TCPSocket_h */
