/*
 *  SocketsPriv.hpp
 *  Sockets
 *
 *  Created by Crazzy on 27.11.15.
 *  Copyright © 2015 Crazzy. All rights reserved.
 *
 */

/* The classes below are not exported */
#pragma GCC visibility push(hidden)

class SocketsPriv
{
	public:
		void HelloWorldPriv(const char *);
};

#pragma GCC visibility pop
