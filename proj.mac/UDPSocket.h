//
//  UDPSocket.h
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//  Copyright © 2015 Crazzy. All rights reserved.
//

#ifndef UDPSocket_h
#define UDPSocket_h

#include <stdio.h>

class UDPSocket : public Socket
{

public:
    UDPSocket( );
    virtual ~UDPSocket( );
    
    virtual bool sendTo(struct sockaddr_in sock_addr_in, BYTEARRAY message);
    virtual bool sendTo(string address, uint16_t port, BYTEARRAY message);
    virtual bool broadcast(uint16_t port, BYTEARRAY message);
    virtual void setBroadcastTarget(string subnet);
    virtual void setDontRoute(bool dont_route);
protected:
    struct in_addr m_broadcastTarget;
};

#endif /* UDPSocket_h */
