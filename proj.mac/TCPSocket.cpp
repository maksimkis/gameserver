//
//  TCPSocket.cpp
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//  Copyright © 2015 Crazzy. All rights reserved.
//

#include "Socket.h"
#include "includes.h"
#include "util.h"
#include "TCPSocket.h"

TCPSocket::TCPSocket():
Socket(),
m_connected(false),
m_lastRecv(getTime()),
m_lastSend(getTime()) {
    allocate(SOCK_STREAM);
    setNonBlock();
}

TCPSocket::TCPSocket(SOCKET socket, struct sockaddr_in sock_addr_in):
Socket(socket, sock_addr_in),
m_connected(true),
m_lastRecv(getTime()),
m_lastSend(getTime()) {
    setNonBlock();
}

TCPSocket::~TCPSocket() {
    
}

void TCPSocket::reset() {
    Socket::reset();
    allocate( SOCK_STREAM );
    m_connected = false;
    m_recvBuffer.clear( );
    m_sendBuffer.clear( );
    m_lastRecv = getTime( );
    m_lastSend = getTime( );
    
    setNonBlock();
    
//    if( !m_LogFile.empty( ) )
//    {
//        ofstream Log;
//        Log.open( m_LogFile.c_str( ), ios :: app );
//        
//        if( !Log.fail( ) )
//        {
//            Log << "----------RESET----------" << endl;
//            Log.close( );
//        }
//    }
}

void TCPSocket::putBytes(string bytes) {
    m_sendBuffer += bytes;
}

void TCPSocket::putBytes(BYTEARRAY bytes) {
    m_sendBuffer += string(bytes.begin(), bytes.end());
}

void TCPSocket::doRecv(fd_set *fd) {
    if(m_socket == INVALID_SOCKET || m_hasError || !m_connected)
        return;
    
    if(FD_ISSET( m_socket, fd))
    {
        // data is waiting, receive it
        char buffer[1024];
        ssize_t c = recv( m_socket, buffer, 1024, 0 );
        
        if(c == SOCKET_ERROR && GetLastError() != EWOULDBLOCK)
        {
            // receive error
            m_hasError = true;
            m_error = GetLastError();
            CONSOLE_Print( "[TCPSOCKET] error (recv) - " + getErrorString());
            return;
        }
        else if(c == 0)
        {
            // the other end closed the connection
            CONSOLE_Print( "[TCPSOCKET] closed by remote host" );
            m_connected = false;
        }
        else if(c > 0)
        {
            // success! add the received data to the buffer
//            if( !m_LogFile.empty( ) ) {
//                ofstream Log;
//                Log.open( m_LogFile.c_str( ), ios :: app );
//                
//                if( !Log.fail( ) )
//                {
//                    Log << "					RECEIVE <<< " << UTIL_ByteArrayToHexString( UTIL_CreateByteArray( (unsigned char *)buffer, c ) ) << endl;
//                    Log.close( );
//                }
//            }
            
            m_recvBuffer += string(buffer, c);
            m_lastRecv = getTime();
        }
    }
}

void TCPSocket::doSend(fd_set *send_fd) {
    if(m_socket == INVALID_SOCKET || m_hasError || !m_connected || m_sendBuffer.empty())
        return;
    
    if(FD_ISSET(m_socket, send_fd)) {
        // socket is ready, send it
        ssize_t s = send(m_socket, m_sendBuffer.c_str(), (int)m_sendBuffer.size(), MSG_NOSIGNAL);
        
        if(s == SOCKET_ERROR && GetLastError() != EWOULDBLOCK) {
            // send error
            m_hasError = true;
            m_error = GetLastError();
            CONSOLE_Print("[TCPSOCKET] error (send) - " + getErrorString());
            return;
        }
        else if( s > 0 )
        {
            // success! only some of the data may have been sent, remove it from the buffer
            
            if( !m_LogFile.empty()) {
//                ofstream Log;
//                Log.open( m_LogFile.c_str( ), ios :: app );
//                
//                if( !Log.fail( ) )
//                {
//                    Log << "SEND >>> " << UTIL_ByteArrayToHexString( BYTEARRAY( m_SendBuffer.begin( ), m_SendBuffer.begin( ) + s ) ) << endl;
//                    Log.close( );
//                }
            }
            
            m_sendBuffer = m_sendBuffer.substr(s);
            m_lastSend = getTime();
        }
    }
}

void TCPSocket::disconnect() {
    if( m_socket != INVALID_SOCKET )
        shutdown( m_socket, SHUT_RDWR );
    m_connected = false;
}

void TCPSocket::setNoDelay(bool noDelay) {
    int OptVal = 0;
    
    if( noDelay )
        OptVal = 1;
    
    setsockopt(m_socket, IPPROTO_TCP, TCP_NODELAY, (const char *)&OptVal, sizeof(int));
}

void TCPSocket::setNonBlock() {
#ifdef WIN32
    int iMode = 1;
    ioctlsocket(m_socket, FIONBIO, (u_long FAR *)&iMode);
#else
    fcntl(m_socket, F_SETFL, fcntl(m_socket, F_GETFL) | O_NONBLOCK);
#endif
}