//
//  UDPSocket.cpp
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//  Copyright © 2015 Crazzy. All rights reserved.
//

#include "Socket.h"
#include "includes.h"
#include "util.h"
#include "UDPSocket.h"


UDPSocket::UDPSocket() : Socket() {
    allocate(SOCK_DGRAM);
    
    // enable broadcast support
    int OptVal = 1;
    setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, (const char *)&OptVal, sizeof(int));
    
    // set default broadcast target
    m_broadcastTarget.s_addr = INADDR_BROADCAST;
}

UDPSocket::~UDPSocket() {
    
}

bool UDPSocket::sendTo(struct sockaddr_in sin, BYTEARRAY message) {
    if(m_socket == INVALID_SOCKET || m_hasError)
        return false;
    
    string messageString = string(message.begin(), message.end());
    
    if(sendto(m_socket, messageString.c_str(), messageString.size(), 0, (struct sockaddr *)&sin, sizeof(sin)) == -1)
        return false;
    
    return true;
}

bool UDPSocket::sendTo(string address, uint16_t port, BYTEARRAY message) {
    if(m_socket == INVALID_SOCKET || m_hasError)
        return false;
    
    // get IP address
    
    struct hostent *HostInfo;
    uint32_t HostAddress;
    HostInfo = gethostbyname(address.c_str());
    
    if(!HostInfo) {
        m_hasError = true;
        // m_Error = h_error;
        CONSOLE_Print("[UDPSOCKET] error (gethostbyname)");
        return false;
    }
    
    memcpy(&HostAddress, HostInfo->h_addr, HostInfo->h_length);
    struct sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = HostAddress;
    sin.sin_port = htons(port);
    
    return sendTo(sin, message);
}

bool UDPSocket::broadcast(uint16_t port, BYTEARRAY message) {
    if(m_socket == INVALID_SOCKET || m_hasError)
        return false;
    
    struct sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = m_broadcastTarget.s_addr;
    sin.sin_port = htons(port);
    
    string MessageString = string(message.begin(), message.end());
    
    if(sendto(m_socket, MessageString.c_str(), MessageString.size(), 0, (struct sockaddr *)&sin, sizeof(sin)) == -1) {
        CONSOLE_Print("[UDPSOCKET] failed to broadcast packet (port " + UTIL_ToString(port) + ", size " + UTIL_ToString(MessageString.size()) + " bytes)");
        return false;
    }
    
    return true;
}

void UDPSocket::setBroadcastTarget(string subnet) {
    if(subnet.empty()) {
        CONSOLE_Print("[UDPSOCKET] using default broadcast target");
        m_broadcastTarget.s_addr = INADDR_BROADCAST;
    }
    else {
        // this function does not check whether the given subnet is a valid subnet the user is on
        // convert string representation of ip/subnet to in_addr
        CONSOLE_Print("[UDPSOCKET] using broadcast target [" + subnet + "]");
        m_broadcastTarget.s_addr = inet_addr(subnet.c_str());
        
        // if conversion fails, inet_addr() returns INADDR_NONE
        
        if(m_broadcastTarget.s_addr == INADDR_NONE) {
            CONSOLE_Print("[UDPSOCKET] invalid broadcast target, using default broadcast target");
            m_broadcastTarget.s_addr = INADDR_BROADCAST;
        }
    }
}

void UDPSocket::setDontRoute(bool dontRoute) {
    int OptVal = 0;
    
    if(dontRoute)
        OptVal = 1;
    
    // don't route packets; make them ignore routes set by routing table and send them to the interface
    // belonging to the target address directly
    
    setsockopt(m_socket, SOL_SOCKET, SO_DONTROUTE, (const char *)&OptVal, sizeof(int));
}