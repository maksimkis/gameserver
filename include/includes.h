//
//  includes.h
//  ConnectServer
//
//  Created by Crazzy on 27.11.15.
//  Copyright © 2015 Crazzy. All rights reserved.
//

#ifndef includes_h
#define includes_h

#ifdef WIN32
#include "ms_stdint.h"
#else
#include <stdint.h>
#endif

#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <vector>

using namespace std;
typedef vector<unsigned char> BYTEARRAY;
typedef pair<unsigned char,string> PIDPlayer;

#ifdef WIN32
#define MILLISLEEP( x ) Sleep( x )
#else
#define MILLISLEEP( x ) usleep( ( x ) * 1000 )
#endif

#undef FD_SETSIZE
#define FD_SETSIZE 512

void CONSOLE_Print( string message );
void DEBUG_Print( string message );
void DEBUG_Print( BYTEARRAY b ) ;

#endif /* includes_h */
