//
//  main.cpp
//  ConnectServer
//
//  Created by Crazzy on 20.11.15.
//  Copyright © 2015 Crazzy. All rights reserved.
//

#include <iostream>
#include <boost/asio.hpp>
#include <boost/date_time.hpp>
#include "includes.h"


using namespace boost;

int main(int argc, const char * argv[]) {
    // insert code here...
        std::cout << "hello connect server\n";
    
    return 0;
}



void CONSOLE_Print( string message )
{
    cout << message << endl;
    
    // logging
    
//    if( !gLogFile.empty( ) )
//    {
//        if( gLogMethod == 1 )
//        {
//            ofstream Log;
//            Log.open( gLogFile.c_str( ), ios :: app );
//            
//            if( !Log.fail( ) )
//            {
//                time_t Now = time( NULL );
//                string Time = asctime( localtime( &Now ) );
//                
//                // erase the newline
//                
//                Time.erase( Time.size( ) - 1 );
//                Log << "[" << Time << "] " << message << endl;
//                Log.close( );
//            }
//        }
//        else if( gLogMethod == 2 )
//        {
//            if( gLog && !gLog->fail( ) )
//            {
//                time_t Now = time( NULL );
//                string Time = asctime( localtime( &Now ) );
//                
//                // erase the newline
//                
//                Time.erase( Time.size( ) - 1 );
//                *gLog << "[" << Time << "] " << message << endl;
//                gLog->flush( );
//            }
//        }
//    }
}

void DEBUG_Print( string message )
{
    cout << message << endl;
}

void DEBUG_Print( BYTEARRAY b )
{
    cout << "{ ";
    
    for( unsigned int i = 0; i < b.size( ); i++ )
        cout << hex << (int)b[i] << " ";
    
    cout << "}" << endl;
}